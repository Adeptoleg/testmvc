# README #

Тестовое задание MVC фреймворк для BeeJee

## Данные проекта ##

* [Репозитарий](https://Adeptoleg@bitbucket.org/Adeptoleg/testmvc.git)
* [Ссылка на проект](https://testjob.pp.ua)

### Установка ###

* git clone https://Adeptoleg@bitbucket.org/Adeptoleg/testmvc.git
* composer dumpautoload -o
* composer upgrade

### Использованные технологии ###

* Composer
* PHP

### Использованные библиотеки ###

* [AltoRouter](https://github.com/dannyvankooten/AltoRouter)
* [Bootstrap](https://getbootstrap.com)
* [PDO](https://packagist.org/packages/slim/pdo)

### Документация ###
* [PHP phpDocumentor](https://docs.phpdoc.org/latest/)
* [PSR4](https://www.php-fig.org/psr/psr-4/)