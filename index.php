<?php
session_start();
require "vendor/autoload.php";

use system\Controller;

/** @var AltoRouter $router */
$router = new AltoRouter();

$router->map('GET|POST','/', 'homeIndex', 'home');
$router->map('GET|POST','/create/', 'homeCreate', 'create');
$router->map('POST','/create/ajax/', 'homeCreateAjax', 'ajax_create');
$router->map('GET|POST','/panel/', 'adminIndex', 'panel_home');
$router->map('POST','/ajax/sort/[i:page]/[name|mail|confirm:field]/[0|1:sort]', 'ajaxSort', 'ajax_sort');
$router->map('POST','/panel/ajax/sort/[i:page]/[name|mail|confirm:field]/[0|1:sort]', 'panelAjaxSort', 'panel_ajax_sort');
$router->map('GET|POST','/panel/task/[i:id]/', 'adminAction', 'admin_action');

/** @var array|false $match */
$match = $router->match();

if(!$match) {
    $match = array(
        'name' => 'home404'
    );
}

/** @var Controller $controller */
$controller = new Controller();
$controllerPage = $match['target'].'Controller';
$controller->$controllerPage($match);