jQuery("#taskForm").submit(function(event){
    event.preventDefault();
    var name = $("#name").val();
    var email = $("#email").val();
    var task = $("#task").val();

    $.ajax({
        type: "POST",
        url: "/create/ajax/",
        data: "name=" + name + "&email=" + email + "&task=" + task,
        success : function(text){
            var request = JSON.parse(text);
            if (request.errorTotal > 0){
                $( "#msgSubmit" )[0].textContent = request.errorMessage;
            } else {
                $( "#msgSubmit" )[0].textContent = 'Запись добавлена';
            }
            $( "#msgSubmit" ).removeClass( "hidden" );
        }
    });
});

