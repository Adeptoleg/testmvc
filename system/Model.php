<?php
namespace system;
use Slim\PDO\Database;
use Slim\PDO\Clause\LimitClause;
/**
 * Class Model
 * @package system
 */
class Model
{
    /** @var Database */
    private $pdo;

    /** @var int */
    private $itemsPerPage;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        /** @var pdo */
        $this->pdo = new Database(
            'mysql:host=localhost;dbname=testmvc;charset=utf8',
            'root',
            ''
        );

        /** @var int itemsPerPage */
        $this->itemsPerPage = 3;
    }

    /**
     * Получаем список задач
     * @param int $page
     * @param string $sortFlag
     * @param bool $ascFlag
     * @return array
     */
    public function getTaskList($page = 1, $sortFlag = 'name', $ascFlag = true)
    {
        $selectStatement = $this->pdo->select(array('Count(id)'))->from('task');
        $stmt = $selectStatement->execute();
        $data = $stmt->fetch();
        $pages = (int) ceil(($data['Count(id)'] / $this->itemsPerPage) );

        If ($pages < $page) {
            $page = $pages;
        }

        if ($pages > 0) {
            $to = $this->itemsPerPage * $page;
            $from = $to - $this->itemsPerPage;
            $selectStatement = $this->pdo->select()
                ->from('task')
                ->limit($this->itemsPerPage, $from)
                ->orderBy(
                        $sortFlag,
                        $this->ascToString($ascFlag)
                    )
            ;
            $stmt = $selectStatement->execute();
            return array(
                'pages' => $pages,
                'items' => $stmt->fetchAll()
            );
        } else {
            return array();
        }
    }

    /**
     * Получаем задачу по айдишке
     * @param $id
     * @return mixed
     */
    public function getTaskById($id) {
        $selectStatement = $this->pdo->select()
            ->from('task')
            ->where('id', '=', $id)
        ;
        $stmt = $selectStatement->execute();
        return $stmt->fetch();
    }

    /**
     * Добавляем задачу
     * @param $name
     * @param $email
     * @param $text
     * @return string
     */
    public function addTask($name, $email, $text) {
        $insertStatement = $this->pdo->insert(array(
            "name" =>$name,
            "mail" => $email,
            "task" => $text
        ))
            ->into("task");
        $insertId = $insertStatement->execute();
        return $insertId;
    }

    /**
     * Обновляем задачу из админки
     * @param $id
     * @param $text
     * @param $confirm
     * @return int
     */
    public function upgradeTask($id, $text, $confirm) {
        if($confirm == 'on') {
            $confirm = 1;
        } else {
            $confirm = 0;
        }

        $text = $this->cleanCode($text);

        $preTask = $this->getTaskById($id);

        $editparam = array(
            "task" => $text,
            "confirm" => $confirm
        );

        if($preTask != $text) {
            $editparam['editadmin'] = 1;
        }

        $updateStatement = $this->pdo->update(
            $editparam
        )
            ->table("task")
            ->where("id", "=", $id);

        return $updateStatement->execute();
    }

    /**
     * В зависиммости от флага проставляем направление фильтрации
     * @param bool $flag
     * @return string
     */
    private function ascToString(bool $flag) {
        if ($flag) {
            return 'ASC';
        } else {
            return 'DESC';
        }
    }

    /**
     * Чистим от пробелов и кода(XSS защита)
     * @param $string
     * @return string
     */
    public function cleanCode($string) {
        return strip_tags(
            stripcslashes($string)
        );
    }
}