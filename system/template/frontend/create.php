
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron d-flex justify-content-center">
    <div id="msgSubmit" class="h3 text-center hidden">Message Submitted!</div>
</div>
<div class="container">
    <form id="taskForm">
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="name" class="h4">Имя пользователя</label>
                <input type="text" class="form-control" id="name" placeholder="Введите Имя" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" class="h4">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Введите email" required>
            </div>
        </div>
        <div class="form-group">
            <label for="task" class="h4 ">Текст задачи</label>
            <textarea id="task" class="form-control" rows="5" placeholder="Введите текст задачи" required></textarea>
        </div>
        <button type="submit" id="form-submit" class="btn btn-success btn-lg pull-right ">Отправить</button>
    </form>
</div>


