<?php
    for ($i = 1; $i <= $layoutItem['param']['totalPage']; $i++) {
?>
        <li class="page-item<?=($i === $layoutItem['param']['currentPage'] ? " active" : "")?>">
            <a class="page-link" href="#"  onclick="currentPage = <?=$i?>;getItems();">
                <?=$i?>
            </a>
        </li>
<?php
    }