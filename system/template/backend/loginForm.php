<div class="jumbotron d-flex justify-content-center">
    <?php
        if(isset($_POST) && !empty($_POST))
        {
            echo '<h3>Введенные данные не верные</h3>';
        }
    ?>
</div>
<div class="container  d-flex justify-content-center">
    <form class="form-signin" method="POST" action="/panel/">
        <h1 class="h3 mb-3 font-weight-normal">Пожалуйста зарегистрируйтесь</h1>
        <label for="inputLogin" class="sr-only">Логин</label>
        <input type="input" name="Login" id="inputLogin" class="form-control" placeholder="Логин" required="" autofocus="">
        <br>
        <label for="inputPassword" class="sr-only">Пароль</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Пароль" required="">
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
    </form>
</div>