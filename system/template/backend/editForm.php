<?php
/** View $this*/
?>
<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron d-flex justify-content-center">
    <form class="form-signout" method="POST" action="/panel/">
        <input type="hidden" name="logout" value="1">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Выйти</button>
    </form>
</div>
<div class="container">
    <hr>
    <div class="row pagination d-flex justify-content-center">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Сортировка по полю
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#" onclick="sortBy = 'name';getItems();">Имя</a>
                <a class="dropdown-item" href="#" onclick="sortBy = 'mail';getItems();">Почта</a>
                <a class="dropdown-item" href="#" onclick="sortBy = 'confirm';getItems();">Статус</a>
            </div>
        </div>
        <div class="dropdown" style="margin-left: 20px;">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Направление соритровки
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#" onclick="ascflag = 1;getItems();">Возростание</a>
                <a class="dropdown-item" href="#" onclick="ascflag = 0;getItems();">Убывание</a>
            </div>
        </div>
    </div>
    <hr>
    <div id="taskItems" class="row pagination d-flex justify-content-center">

    </div>
    <hr>
    <nav aria-label="Task navigation">
        <ul id="taskList" class="pagination d-flex justify-content-center">

        </ul>
    </nav>
</div> <!-- /container -->
<script language="JavaScript">
    var currentPage = 1;
    var ascflag = 0;
    var sortBy = 'name';
    window.onload = function() {
        getItems();
    };

    function getItems()
    {
        $( "#taskItems" )[0].innerHTML = '<h1 class="">Загрузка</h1>';
        $.ajax({
            type: "POST",
            url: "/panel/ajax/sort/" + currentPage + "/" + sortBy + "/" + ascflag,
            data: "",
            success : function(text){
                var request = JSON.parse(text);
                if (request.logout > 0){
                    Location.reload(true);
                }
                $( "#taskItems" )[0].innerHTML = request.result;
                $( "#taskList" )[0].innerHTML = request.paginator;
            }
        });
    }
</script>