<div class="jumbotron d-flex justify-content-center">
    <form class="form-signout" method="POST" action="/panel/">
        <input type="hidden" name="logout" value="1">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Выйти</button>
    </form>
</div>
<div class="container">
    <form id="taskFormUpgrade" method="POST">
        <div class="form-group">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <input type="checkbox" name="confirm" aria-label="Отметить как выполенную." <?=($layoutItem['param']['confirm'] > 0 ? "checked" : "")?>>
                    </div>
                </div>
                Отметить как выполенную.
            </div>
            <label for="task" class="h4 ">Текст задачи</label>
            <textarea id="task" name="task" class="form-control" rows="5" placeholder="Введите текст задачи" required><?=$layoutItem['param']['task'] ?></textarea>
        </div>
        <button type="submit" id="form-submit" class="btn btn-success btn-lg pull-right ">Отправить</button>
    </form>
</div>
