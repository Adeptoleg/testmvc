<?php
namespace system;

use system\Model;
use system\View;

/**
 * Class Controller
 * @package system
 */
class Controller
{
    /**
     * Класс модели
     * @var \system\Model
     */
    private $model;

    /**
     * Класс вида
     * @var \system\View
     */
    private $view;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        /** @var Model model */
        $this->model = new Model();
        /** @var View view */
        $this->view = new View();
    }

    public function homeIndexController($params)
    {
        $this->view->addBlock(
            'head'
        );
        $this->view->addBlock(
            'home',
            'frontend',
            $params
        );
        $this->view->addBlock(
            'footer'
        );
        echo $this->view->renderBlocks(true);
    }

    public function homeCreateController($params)
    {
        $pages = $this->model->getTaskList();

        $this->view->addBlock(
            'head'
        );
        $this->view->addBlock(
            'create',
            'frontend',
            $params
        );
        $this->view->addBlock(
            'footer'
        );
        echo $this->view->renderBlocks(true);
    }

    public function homeCreateAjaxController($params)
    {
        $errorTotal = 0;
        $errorMessage = '';

        if (!preg_match("/^[a-zA-Z0-9 ]*$/",$_POST['name'])) {
            ++$errorTotal;
            $errorMessage .= " В именни разрешены только буквы и цыфры.";
        }

        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            ++$errorTotal;
            $errorMessage .= " Неверный формат електронной почты";
        }

        $_POST['task'] = $this->model->cleanCode($_POST['task']);

        if ($errorTotal) {
            $return = json_encode(
                array(
                    'errorTotal' => $errorTotal,
                    'errorMessage' => $errorMessage
                )
            );
        } else {
            $this->model->addTask(
                $_POST['name'],
                $_POST['email'],
                $_POST['task']
            );
            $return = json_encode(
                array(
                    'errorTotal' => 0
                )
            );
        }

        $this->view->addBlock(
            'ajax',
            'frontend',
            $return
        );
        echo $this->view->renderBlocks(true);
    }

    public function adminIndexController($params)
    {
        $this->loginform();

        $this->view->addBlock(
            'head',
            'backend'
        );

        if(!$_SESSION['login']) {
            $this->view->addBlock(
                'loginForm',
                'backend'
            );
        } else {
            $this->view->addBlock(
                'editForm',
                'backend'
            );
        }


        $this->view->addBlock(
            'footer'
        );

        echo $this->view->renderBlocks(true);
    }

    public function ajaxSortController($params)
    {
        $return = array(
            'result' => ''
        );

        $taskLists = $this->model->getTaskList(
            $params['params']['page'],
            $params['params']['field'],
            $params['params']['sort']
        );

        foreach ($taskLists['items'] as $taskList){
            $return['result'] .= $this->view->renderBlock(
                array(
                    'name' => 'taskItem',
                    'theme' => 'frontend',
                    'param' => $taskList
                )
            );
        }
        $return['paginator'] .= $this->view->renderBlock(
            array(
                'name' => 'paginator',
                'theme' => 'frontend',
                'param' => array(
                    'currentPage' => $params['params']['page'],
                    'totalPage' => $taskLists['pages']
                )
            )
        );

        $this->view->addBlock(
            'ajax',
            'frontend',
            json_encode($return)
        );
        echo $this->view->renderBlocks(true);
    }

    public function adminActionController($params)
    {
        $this->loginform();

        $this->view->addBlock(
            'head',
            'backend'
        );

        if(!$_SESSION['login']) {
            $this->view->addBlock(
                'loginForm',
                'backend'
            );
        } else {
            if(isset($_POST['confirm']) || isset($_POST['task']))
            {
                $this->model->upgradeTask($params['params']['id'], $_POST['task'], $_POST['confirm']);
            }

            /** @var array $value */
            $value = $this->model->getTaskById($params['params']['id']);

            $this->view->addBlock(
                'edit',
                'backend',
                $value
            );
        }


        $this->view->addBlock(
            'footer'
        );

        echo $this->view->renderBlocks(true);
    }

    public function panelAjaxSortController($params)
    {
        $this->loginform();
        $return = array(
            'result' => '',
            'logout' => 0
        );
        if($_SESSION['login']){
            $taskLists = $this->model->getTaskList(
                $params['params']['page'],
                $params['params']['field'],
                $params['params']['sort']
            );

            foreach ($taskLists['items'] as $taskList){
                $return['result'] .= $this->view->renderBlock(
                    array(
                        'name' => 'taskItem',
                        'theme' => 'backend',
                        'param' => $taskList
                    )
                );
            }
            $return['paginator'] .= $this->view->renderBlock(
                array(
                    'name' => 'paginator',
                    'theme' => 'frontend',
                    'param' => array(
                        'currentPage' => $params['params']['page'],
                        'totalPage' => $taskLists['pages']
                    )
                )
            );
        } else {
            $return['logout'] = 1;
        }


        $this->view->addBlock(
            'ajax',
            'frontend',
            json_encode($return)
        );
        echo $this->view->renderBlocks(true);
    }

    private function loginform(){
        if(isset($_POST['Login']) && $_POST['Login'] == 'admin' && $_POST['password'] == '123' || $_SESSION['login']) {
            $_SESSION['login'] = true;
        }

        if(isset($_POST['logout'])){
            $_SESSION['login'] = false;
        }
    }
}