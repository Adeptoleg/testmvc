<?php
namespace system;

use function Composer\Autoload\includeFile;

/**
 * Class View
 * @package system
 */
class View
{
    /**
     * Список блоков вывода
     * @var array
     */
    private $layout;

    /**
     * Обработанный вывод
     * @var string
     */
    private $renderResult;

    /**
     * Папка с темплейтами
     * @var string
     */
    private $rootDir;

    /**
     * View constructor.
     */
    public function __construct()
    {
        /** @var array layout */
        $this->layout = array();

        /** @var string renderResult */
        $this->renderResult = '';

        $this->rootDir = 'system' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR;
    }

    /**
     * Добавляет блок в очередь на отрисовку
     * @param $name
     * @param string $theme
     * @param array $param
     */
    public function addBlock($name, $theme = 'frontend', $param = array())
    {
        $this->layout[] = array(
            'name' => $name,
            'theme' => $theme,
            'param' => $param
        );
    }

    /**
     * Рендерит блок
     * @param $layoutItem layout
     * @return false|string
     */
    public function renderBlock($layoutItem)
    {
        ob_start();
        include (
            $this->rootDir . $layoutItem['theme'] . DIRECTORY_SEPARATOR . $layoutItem['name'] . '.php'
        );
        return ob_get_clean();
    }

    /**
     * Класс рендера всех блоков
     */
    public function renderBlocks($echoFlag = false)
    {
        foreach ($this->layout as $layoutItem) {
            $this->renderResult .= $this->renderBlock($layoutItem);
        }

        if ($echoFlag) {
            echo $this->renderResult;
        }
    }
}